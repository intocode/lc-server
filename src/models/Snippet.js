import mongoose from 'mongoose';

const snippetSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },

    level: {
      type: String,
      required: true,
    },

    description: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

export const Snippet = mongoose.model('Snippet', snippetSchema);
