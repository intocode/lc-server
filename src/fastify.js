import Fastify from 'fastify';
import autoload from 'fastify-autoload';
import cors from 'fastify-cors';
import { fileURLToPath } from 'url';
import { join, dirname } from 'path';
import schemas from './schemas/index.js';

const fastify = Fastify({
  logger: true,
});

(async () => {
  /**
   * Активация CORS
   */
  fastify.register(cors);

  /**
   * Автозагрузка роутов
   *
   * url: https://github.com/fastify/fastify-autoload
   */
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = dirname(__filename);

  await fastify.register(autoload, {
    options: { prefix: '/api/v0' },
    dir: join(__dirname, 'services'),
  });

  /**
   * Проверочный роут /
   */
  fastify.get('/', schemas.main, async () => ({ message: 'hi' }));
})();

export default fastify;
