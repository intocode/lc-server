import mongoose from 'mongoose';
import dotenv from 'dotenv';
import fastify from './fastify.js';

dotenv.config();

(async () => {
  try {
    await mongoose.connect(process.env.MONGO_DEV);
    const address = await fastify.listen(process.env.PORT);

    console.log(`Сервер успешно запущен: ${address}`);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
